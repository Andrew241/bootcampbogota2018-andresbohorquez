package Utilidades;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FireFoxTestDriver {
	private WebDriver driver;
	public FireFoxTestDriver () {
		System.setProperty("webdriver.gecko.driver","Drivers/geckodriver");
		driver= new FirefoxDriver();
	}
	
	public WebDriver getDriver() {
		return this.driver;
	}
}
