package Utilidades;

import org.junit.Test;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import junit.framework.Assert;

public class TravelocityHomePage extends BasePage{
	//constructores
	public TravelocityHomePage(WebDriver pDriver) {
		super(pDriver);
		// TODO Auto-generated constructor stub
	}
	public TravelocityHomePage(WebDriver pDriver,int timeOutInSec) {
		super(pDriver);
		// TODO Auto-generated constructor stub
	}
	//locators
	@FindBy(id="tab-flight-tab-hp")
	private WebElement flightButton;
	
	@FindBy(id="flight-type-roundtrip-label-hp-flight")
	private WebElement roundTripButton;
	
	@FindBy(id="flight-origin-hp-flight")
	private WebElement originCity;
	
	@FindBy(id="flight-destination-hp-flight")
	private WebElement destinationCity;
	
	@FindBy(id="flight-departing-hp-flight")
	private WebElement departureDate;
	
	@FindBy(id="flight-returning-hp-flight")
	private WebElement returnDate;
	
	@FindBy(id="flight-adults-hp-flight")
	private WebElement adultsNumber;
	
	@FindBy(id="flight-children-hp-flight")
	private WebElement childrenNumber;
	
	@FindBy(xpath=("//*[/html/body/section/div/div/div/div[3]/div/div[1]/div/section[1]/form/div[8]/label/button]"))
	private WebElement searchButton;
	
	@FindBy (id="ajax-error")
	private WebElement ajaxError;
	
	@FindBy (id="flights-advanced-options-toggle")
	private WebElement advancedOptionsToggle;
	
	//methods
	public void clickOnFlightButton() {
		flightButton.click();
	}
	public void clickOnSearchButton() {
		searchButton.click();
	}
	public WebElement getFlightButton() {
		return flightButton;
	}
	public WebElement getRoundTripButton() {
		return roundTripButton;
	}
	public WebElement getOriginCity() {
		return originCity;
	}
	public WebElement getDestinationCity() {
		return destinationCity;
	}
	public WebElement getDepartureDate() {
		return departureDate;
	}
	public WebElement getReturnDate() {
		return returnDate;
	}
	public WebElement getAdultsNumber() {
		return adultsNumber;
	}
	public WebElement getChildrenNumber() {
		return childrenNumber;
	}
	public WebElement getSearchButton() {
		return searchButton;
	}
	public WebElement getAjaxError() {
		return ajaxError;
	}
	public WebElement getAdvancedOptionsToggle() {
		return advancedOptionsToggle;
	}
	
	public void clickFlight(String originCity,String destinationCity,String departureDate,String returnDate ) throws InterruptedException {  
		
		
		 clickOnFlightButton();
		 this.getWait().until(ExpectedConditions.elementToBeClickable(this.getRoundTripButton()));
		 this.getOriginCity().clear();
		 this.getOriginCity().sendKeys(originCity);
		 this.getOriginCity().sendKeys(Keys.ENTER);
		 this.getDestinationCity().clear();
		 this.getDestinationCity().sendKeys(destinationCity);
		 this.getDestinationCity().sendKeys(Keys.ENTER);
		 this.getDepartureDate().clear();
		 this.getDepartureDate().sendKeys(departureDate);
		 this.getReturnDate().clear();
		 this.getReturnDate().sendKeys(returnDate);
		 this.getDepartureDate().sendKeys(Keys.ENTER);
		 this.clickOnSearchButton();
		 this.getWait().until(ExpectedConditions.elementToBeClickable(this.getAdvancedOptionsToggle()));

		 
	  }
	@Test
	public void isInTravelocity() {
		//assertequals();
		//this.getDriver().getCurrentUrl(),"https://www.travelocity,com/");
	}
	

	
}
	

	
	

