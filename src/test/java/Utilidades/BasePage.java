package Utilidades;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {
	private WebDriver driver;
	private WebDriverWait wait;
	
	public BasePage(WebDriver pDriver) {
		PageFactory.initElements(pDriver, this);
		wait= new WebDriverWait(pDriver,10);
		driver=pDriver;
	}
	public BasePage(WebDriver pDriver,int timeOutInSec) {
		PageFactory.initElements(pDriver, this);
		wait= new WebDriverWait(pDriver,timeOutInSec);
		driver=pDriver;
	}
	public WebDriverWait getWait() {
		return this.wait;
	}
	public WebDriver getDriver() {
		return this.driver;
	}
	public void dispose() {
		if(driver!=null) {
			driver.quit();
		}
	}
	
	
}
