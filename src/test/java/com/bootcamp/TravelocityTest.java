package com.bootcamp;

import org.testng.annotations.Test;

import Utilidades.FireFoxTestDriver;
import Utilidades.TravelocityHomePage;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

	


public class TravelocityTest extends BaseTest{

	
	
	
	@Test(dataProvider="datosVuelo")
	public void clickFlight(String originCity,String destinationCity,String departureDate,String returnDate ) throws InterruptedException {  
		
	 TravelocityHomePage home = getTavelocityHomePage();
	 home.clickOnFlightButton();
	 home.getWait().until(ExpectedConditions.elementToBeClickable(home.getRoundTripButton()));
	 home.getOriginCity().clear();
	 home.getOriginCity().sendKeys(originCity);
	 home.getOriginCity().sendKeys(Keys.ENTER);
	 home.getDestinationCity().clear();
	 home.getDestinationCity().sendKeys(destinationCity);
	 home.getDestinationCity().sendKeys(Keys.ENTER);
	 home.getDepartureDate().clear();
	 home.getDepartureDate().sendKeys(departureDate);
	 home.getReturnDate().clear();
	 home.getReturnDate().sendKeys(returnDate);
	 home.getDepartureDate().sendKeys(Keys.ENTER);
	 home.clickOnSearchButton();
	 home.getWait().until(ExpectedConditions.elementToBeClickable(home.getAdvancedOptionsToggle()));
	 Assert.assertEquals(home.getAjaxError().getAttribute("class"), "hide box");
	 home.getDriver().navigate().back();
	 
  }
	@DataProvider(name = "datosVuelo")
	  public static Object[][] datosVuelo() {
	 
	        return new Object[][] {{ "Bogota","Singapore","07/07/2018","07/22/2018"}, { "Singapore", "Tokio","07/07/2018","07/22/2018" }};
	 
	  }

}
