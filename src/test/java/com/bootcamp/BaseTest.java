package com.bootcamp;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Utilidades.FireFoxTestDriver;
import Utilidades.TravelocityHomePage;



public class BaseTest {

	protected FireFoxTestDriver driver;
	private TravelocityHomePage travelocityHome;
	
	@BeforeClass
	protected void beforeTest() {
		driver = new FireFoxTestDriver();
		driver.getDriver().get("https://www.travelocity.com/");

		travelocityHome = new TravelocityHomePage(driver.getDriver());
	}
	@AfterClass
	protected void afterTest() {
		this.driver.getDriver().quit();
		
	}
	protected TravelocityHomePage getTavelocityHomePage() {
		return travelocityHome;
	}
	
}
